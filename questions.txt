Welcome to the first Git exercise!
Completing these prompts will help you get more comfortable with
Git.

1. What is your favorite color?

***
  Remember to:
    - add
    - commit
    - push
  your answer before answering the next question!
***

2. What is your favorite food?

My favorite  food is pasta.
3. Who is your favorite fictional character?
Robin hood is my favorite fictional character.

4. What is your favorite animal?
Dove bird, because they represent purity.

5. What is your favorite programming language? (Hint: You can always say Python!!)
Python, because it is  readable.
